/*******************************************************************************
* Copyright (c) 2005, 2006 QFS GmbH. All rights reserved.
* The contents of this file are made available under the terms
* of the GNU Lesser General Public License (LGPL) Version 2.1 that
* accompanies this distribution (lgpl-v21.txt).  The LGPL is also
* available at http://www.gnu.org/licenses/lgpl.html.  If the version
* of the LGPL at http://www.gnu.org is different to the version of
* the LGPL accompanying this distribution and there is any conflict
* between the two license versions, the terms of the LGPL accompanying
* this distribution shall govern.
*
* Contributors:
 *     QFS GmbH - Accessibility/Testing hooks
*******************************************************************************/

#ifndef GTK_WIDGET_VISIBLE
#define GTK_WIDGET_VISIBLE(arg0) 0
#endif
#if ! GTK_CHECK_VERSION(3,0,0)
#define GTK_WIDGET_HEIGHT(arg0) (arg0)->allocation.height
#define GTK_WIDGET_WIDTH(arg0) (arg0)->allocation.width
#define GTK_WIDGET_WINDOW(arg0) (arg0)->window
#define GTK_WIDGET_X(arg0) (arg0)->allocation.x
#define GTK_WIDGET_Y(arg0) ((GtkWidget *)arg0)->allocation.y
#endif
