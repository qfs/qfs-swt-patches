/*******************************************************************************
* Copyright (c) 2005, 2006 QFS GmbH. All rights reserved.
* The contents of this file are made available under the terms
* of the GNU Lesser General Public License (LGPL) Version 2.1 that
* accompanies this distribution (lgpl-v21.txt).  The LGPL is also
* available at http://www.gnu.org/licenses/lgpl.html.  If the version
* of the LGPL at http://www.gnu.org is different to the version of
* the LGPL accompanying this distribution and there is any conflict
* between the two license versions, the terms of the LGPL accompanying
* this distribution shall govern.
*
* Contributors:
 *     QFS GmbH - Accessibility/Testing hooks
*******************************************************************************/
#include "os_structs.h"

#ifndef NO_XMotionEvent
void cacheXMotionEventFields(JNIEnv *env, jobject lpObject);
XMotionEvent *getXMotionEventFields(JNIEnv *env, jobject lpObject, XMotionEvent *lpStruct);
void setXMotionEventFields(JNIEnv *env, jobject lpObject, XMotionEvent *lpStruct);
#define XMotionEvent_sizeof() sizeof(XMotionEvent)
#else
#define cacheXMotionEventFields(a,b)
#define getXMotionEventFields(a,b,c) NULL
#define setXMotionEventFields(a,b,c)
#define XMotionEvent_sizeof() 0
#endif

#ifndef NO_XKeyEvent
void cacheXKeyEventFields(JNIEnv *env, jobject lpObject);
XKeyEvent *getXKeyEventFields(JNIEnv *env, jobject lpObject, XKeyEvent *lpStruct);
void setXKeyEventFields(JNIEnv *env, jobject lpObject, XKeyEvent *lpStruct);
#define XKeyEvent_sizeof() sizeof(XKeyEvent)
#else
#define cacheXKeyEventFields(a,b)
#define getXKeyEventFields(a,b,c) NULL
#define setXKeyEventFields(a,b,c)
#define XKeyEvent_sizeof() 0
#endif

